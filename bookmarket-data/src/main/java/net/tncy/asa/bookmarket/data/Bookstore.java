package net.tncy.asa.bookmarket.data;

import java.util.ArrayList;
import java.util.List;

public class Bookstore {
    protected List<InventoryEntry> inventory;

    public Bookstore() {
        this.inventory = new ArrayList<>();
    }

    public void addBook(InventoryEntry i) {
        this.inventory.add(i);
    }

    public List<InventoryEntry> getInventory() {
        return inventory;
    }
}
