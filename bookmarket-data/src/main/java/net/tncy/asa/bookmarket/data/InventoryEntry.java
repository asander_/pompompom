package net.tncy.asa.bookmarket.data;

public class InventoryEntry {
    protected int count;
    protected Book book;

    public InventoryEntry(int count, Book book) {
        this.count = count;
        this.book = book;
    }

    public int getCount() {
        return count;
    }

    public InventoryEntry setCount(int count) {
        this.count = count;
        return this;
    }

    public Book getBook() {
        return book;
    }

    public InventoryEntry setBook(Book book) {
        this.book = book;
        return this;
    }
}
