package net.tncy.asa.bookmarket.data;

import net.tncy.asa.validator.ISBN;

public class Book {
    protected String title;
    protected String subtitle;
    protected String author;
    protected String edition;
    @ISBN
    protected String isbn;
    protected float price;

    public Book(String title, String subtitle, String author, String edition, String isbn, float price) {
        this.title = title;
        this.subtitle = subtitle;
        this.author = author;
        this.edition = edition;
        this.isbn = isbn;
        this.price = price;
    }

    public String getTitle() {
        return title;
    }

    public Book setTitle(String title) {
        this.title = title;
        return this;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public Book setSubtitle(String subtitle) {
        this.subtitle = subtitle;
        return this;
    }

    public String getAuthor() {
        return author;
    }

    public Book setAuthor(String author) {
        this.author = author;
        return this;
    }

    public String getEdition() {
        return edition;
    }

    public Book setEdition(String edition) {
        this.edition = edition;
        return this;
    }

    public float getPrice() {
        return price;
    }

    public Book setPrice(float price) {
        this.price = price;
        return this;
    }
}
